module.exports={
    entry:'./src/main.js',       //指定打包入口文件
    output:{
        path: __dirname+'/dist', //webpack 1.14.0 要求 这个路径是一个绝对路径
        filename:'build.js'     //编译后的存放文件名
    },
     //导入CSSloader模块
     module:{
        loaders:[
            {
                test: /\.css$/,
                loader:'style-loader!css-loader'
            },
        ]
    }
}